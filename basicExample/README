--------------------------------------------------------------------------------------------------
    __  ________ __  __         ___   __  __________  __   _________            __
   /  |/  / ___// / / /  ____  /   | / / / /_  __/ / / /  / ____/ (_)__  ____  / /_
  / /|_/ /\__ \/ / / /  / __ \/ /| |/ / / / / / / /_/ /  / /   / / / _ \/ __ \/ __/
 / /  / /___/ / /_/ /  / /_/ / ___ / /_/ / / / / __  /  / /___/ / /  __/ / / / /_
/_/  /_//____/\____/   \____/_/  |_\____/ /_/ /_/ /_/   \____/_/_/\___/_/ /_/\__/

--------------------------------------------------------------------------------------------------
                                   Basic Example README
--------------------------------------------------------------------------------------------------

    This example ColdFusion application is meant to demonstrate how to use MSU's oAuth
    authentication mechanism to allow your users to participate in the Single-Sign-On system
    without having to utilize complex web-server modules like Shibboleth, Kerberos or Sentinel.
    It uses the userAuth.cfc component to manage the login and login properties returned by the
    oAuth system.  It is fully oAuth 2.0 compatible (so it should work with other systems such as
    Google's oAuth system with minimal changes).

    The CFC is compatible with Adobe ColdFusion 8.0.1, 9.x, 10.x and Railo 3.1+, 4.x.  It has been
    tested on Adobe ColdFusion 8.0.1, 9.0 and Railo 4.1.  Due to the use of the http client, it is
    NOT compatible with the BlueDragon engine.

    This example is a "bare bones" that simply shows the authentication system and nothing more.
    More complicated examples should be included in this package to demonstrate a full application
    that you can use as a template for your CF applications.

    To use this sample:
    -------------------

    -  Apply to be an oAuth client application from IT Services (formerly AIS).  There will be a
       questionnaire that you will need to fill out.

    -  Edit the Application.cfc
           - Set the application name and application timeout.
           - Review the publicPages variable.  Make sure these paths represent pages that you don't want
             to protect.  You *must* include the callback page you provided IT Services.  If your callback
             page is http://www.msu.edu/oauth/callback.cfm, your paths should include /oauth/callback.cfm

    -  Edit the ./cfcs/userAuth.cfc
           - Set the oauth.clientID, oauth.secret and oauth.callback variables to match what was provided
             by IT Services.
           - (optional) Update the variables passed back by the oAuth server, if you requested additional
             properties. The default ones are the base variables included in all requests.

    -  Run the sample.  Browse to the index.cfm page.  You should get a note returned that you are not
       logged in.  Click the login link, which should send you to the login.msu.edu server.  After a
       successful login, you should be sent back to your server.  If things worked right, you should see
       your name on the index page.


    How it works:
    -------------

    -  When somebody browses to your application, the application.cfc (or for older applications, the
       application.cfm) is parsed.
       -  If a session does not already exist, one is created by running the onSessionStart function. We
          have customized this function so that it creates an instance of the "session.user" variable.
          This variable is an instance of our oAuth authentication CFC.  We then tell the component to
          initialize itself with the defaults (logged out).
       -  Before each page is read in and processed, the function onRequestStart is run.  We've customized
          this function to check to see if the page the user requested needs to be protected behind a login
          screen.  If the page is "public" we continue processing normally.  If it needs to be protected,
          we call the oAuth component's "forceLogin" function, which will send the user to the login screen
          if they are not already logged in.
    -  During the login attempt. the login server will then send the user back to the callback page.  This
       page needs to do two things :  (a) verify that our token we passed the login server is what is being
       passed back to us (the request was not hijacked)  and (b) capture the login token for further
       processing.  The callback page needs to call the session.user.processLogin() function and pass in the
       "state" (url.state), and the login token (url.code).  This function will pass back either true for a
       successful login, or false for a failed login.  The callback page should check this result and display
       and error to the user if the login was not successful.
    -  After the login is successful, the session.user variable is populated with the user information that
       was passed back from the oAuth server.  This includes the name, email address, and a unique identifier.
       These are available in the session.user.name, session.user.first_name, etc. variables.


   License:
   --------
    Copyright 2013, Michigan State University, Board of Trustees

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Released by Nick Kwiatkowski (nk@msu.edu), as an acting agent of MSU


